import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import QueryRenderer from './QueryRenderer'
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <QueryRenderer></QueryRenderer>
        <Text>Open up App.js to start working on your app!</Text>
        <Button title="What should be written here?" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
